output "addressRange" {
  description = "Address range of virtual network"
  value = azurerm_virtual_network.virtualNnetwork.address_space
}

output "subnets" {
  description = "virtual network subnet"
  value = azurerm_virtual_network.virtualNnetwork.subnet
}
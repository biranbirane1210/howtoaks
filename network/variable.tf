variable "location" {
  description = "The location of our network"
  type        = string
  default     = "northeurope"
}

variable "virtualNetworkName" {
  description = "The name of our network"
  type        = string
  default     = "dev-ne-vnt-howtoaks-tf"
}

variable "virtualNetworkAddressSpace" {
  description = ""
  type        = string
  default     = "10.1.0.0/20"
}

variable "ipv6Enabled"{
  description = ""
  type        = bool
  default     = false
}

variable "subnetCount"{
  description = "number of subnet"
  type        = number
  default     = 3
}

variable "subnetAzureKubernetesService"{
  description = "number of subnet in our k8s cluster"
  type        = string
  default     = "dev-ne-snt-aks-howtoaks-tf"
}

variable "subnetAzureKubernetesServiceAddressRange"{
  description = "Address Range in our k8s cluster"
  type        = string
  default     = "10.1.0.0/21"
}

variable "subnetApplicationGateway"{
  description = "Gateway subnet"
  type        = string
  default     = "dev-ne-snt-agw-howtoaks-tf"
}

variable "subnetApplicationGatewayAddressRange"{
  description = "Gateway address range"
  type        = string
  default     = "10.1.8.0/27"
}

variable "subnetBastion"{
  description = "Bastion subnet"
  type        = string
  default     = "AzureBastionSubnet"
}

variable "subnetBastionAddressRange"{
  description = "Bastion address range"
  type        = string
  default     = "10.1.8.32/27"
}

variable "bastionName"{
  description = "Bastion address range"
  type        = string
  default     = "dev-ne-bastion-howtoaks-tf"
}

variable "publicIpAddressForBastion"{
  description = "Bastion public ip address "
  type        = string
  default     = "dev-ne-pip-bastion-howtoaks-tf"
}

variable "networkSecurityGroupBastion"{
  description = "Bastion network security group "
  type        = string
  default     = "dev-ne-nsg-bastion-howtoaks-tf"
}

variable "subnetManagement"{
  description = "subnnet management "
  type        = string
  default     = "dev-ne-snt-management-howtoaks-tf"
}

variable "subnetManagementAddressRange"{
  description = "subnnet management address range"
  type        = string
  default     = "10.1.8.64/27"
}

variable "networkSecurityGroupManagement"{
  description = "network security group management"
  type        = string
  default     = "dev-ne-nsg-management-howtoaks-tf"
}


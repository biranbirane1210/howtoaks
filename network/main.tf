# Azure Provider source and version being used
terraform {
  backend "http" {}

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "network" {
  location = var.location
  name     = "network-resource"
}

resource "azurerm_network_security_group" "networkSecurityGroupBastion" {
  location            = azurerm_resource_group.network.location
  name                = var.networkSecurityGroupBastion
  resource_group_name = azurerm_resource_group.network.name

}

resource "azurerm_network_security_rule" "Allow_Inbound_GatewayManager" {
  description                 = "Allow Ingress Traffic from Azure Bastion Gateway Manager"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix        = "GatewayManager"
  destination_address_prefix   = "*"
  access                      = "Allow"
  priority                    = 100
  direction                   = "Inbound"
  name                        = "Allow_Inbound_GatewayManager"
  network_security_group_name = azurerm_network_security_group.networkSecurityGroupBastion.name
  resource_group_name         = azurerm_resource_group.network.name
}

resource "azurerm_network_security_rule" "Allow_Inbound_AzureCloud" {
  description                 = "Allow Inbound from AzureCloud"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix        = "AzureCloud"
  destination_address_prefix   = "*"
  access                      = "Allow"
  priority                    = 110
  direction                   = "Inbound"
  name                        = "Allow_Inbound_AzureCloud"
  network_security_group_name = azurerm_network_security_group.networkSecurityGroupBastion.name
  resource_group_name         = azurerm_resource_group.network.name
}

resource "azurerm_network_security_rule" "Allow_Https_Inbound" {
  description                 = "Allow Https through port 443 from the Internet"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix        = "Internet"
  destination_address_prefix   = "*"
  access                      = "Allow"
  priority                    = 120
  direction                   = "Inbound"
  name                        = "Allow_Https_Inbound"
  network_security_group_name = azurerm_network_security_group.networkSecurityGroupBastion.name
  resource_group_name         = azurerm_resource_group.network.name
}

resource "azurerm_network_security_rule" "Allow_SSH_Outbound" {
  description                 = "Egress Traffic to target VMs: Azure Bastion will reach the target VMs over private IP and SSH port"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix        = "*"
  destination_address_prefix   = "VirtualNetwork"
  access                      = "Allow"
  priority                    = 100
  direction                   = "Outbound"
  name                        = "Allow_SSH_Outbound"
  network_security_group_name = azurerm_network_security_group.networkSecurityGroupBastion.name
  resource_group_name         = azurerm_resource_group.network.name
}

resource "azurerm_network_security_rule" "Allow_OutBound_AzureCloud" {
  description                 = "Egress Traffic to other public endpoints in Azure"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix        = "*"
  destination_address_prefix   = "AzureCloud"
  access                      = "Allow"
  priority                    = 120
  direction                   = "Outbound"
  name                        = "Allow_OutBound_AzureCloud"
  network_security_group_name = azurerm_network_security_group.networkSecurityGroupBastion.name
  resource_group_name         = azurerm_resource_group.network.name
}


resource "azurerm_network_security_group" "networkSecurityGroupManagement" {
  location            = var.location
  name                = var.networkSecurityGroupManagement
  resource_group_name = azurerm_resource_group.network.name
}

resource "azurerm_network_security_rule" "Allow_RDP_SSH_From_AzureBastion" {
  description                 = "Ingress Traffic from Azure Bastion"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_ranges     = ["3389", "22"]
  source_address_prefix        = var.subnetBastionAddressRange
  destination_address_prefix   = "*"
  access                      = "Allow"
  priority                    = 100
  direction                   = "Inbound"
  name                        = "Allow_RDP_SSH_From_AzureBastion"
  network_security_group_name = azurerm_network_security_group.networkSecurityGroupManagement.name
  resource_group_name         = azurerm_resource_group.network.name
}



resource "azurerm_virtual_network" "virtualNnetwork" {
  name                = var.virtualNetworkName
  location            = var.location
  depends_on          = [azurerm_network_security_group.networkSecurityGroupBastion,azurerm_network_security_group.networkSecurityGroupManagement]
  address_space       = [var.virtualNetworkAddressSpace]
  subnet {
    address_prefix  = var.subnetAzureKubernetesServiceAddressRange
    name           = var.subnetAzureKubernetesService
  }
  subnet {
    address_prefix  = var.subnetApplicationGatewayAddressRange
    name           = var.subnetApplicationGateway
  }

  subnet{
    address_prefix  = var.subnetManagementAddressRange
    name           = var.subnetManagement
    security_group = azurerm_network_security_group.networkSecurityGroupManagement.id
  }
  resource_group_name = azurerm_resource_group.network.name
}

resource "azurerm_subnet" "subnetBastion" {
  name                 = var.subnetBastion
  resource_group_name  = azurerm_resource_group.network.name
  virtual_network_name = azurerm_virtual_network.virtualNnetwork.name
  address_prefixes      = [var.subnetBastionAddressRange]
}



resource "azurerm_public_ip" "publicIpAddressForBastion" {
  name                = var.publicIpAddressForBastion
  location            = var.location
  sku                 = "Standard"
  allocation_method   = "Static"
  resource_group_name = azurerm_resource_group.network.name
}





resource "azurerm_bastion_host" "bastionHosts" {
  name                = var.bastionName
  location            = var.location
  depends_on = [azurerm_virtual_network.virtualNnetwork,azurerm_public_ip.publicIpAddressForBastion]
  ip_configuration {
    name                 = "IpConf"
    public_ip_address_id = azurerm_public_ip.publicIpAddressForBastion.id
    subnet_id            = azurerm_subnet.subnetBastion.id
  }

  resource_group_name = azurerm_resource_group.network.name
}

